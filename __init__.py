# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import department

def register():
    Pool.register(
        department.Department,
        department.DepartmentUser,
        module='company_department', type_='model')
